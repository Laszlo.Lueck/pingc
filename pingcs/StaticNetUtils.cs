using System;
using System.Runtime;

namespace pingCs;

internal static class StaticNetUtils
{
    public static readonly Func<byte, IpAddressFamilyEnum, string> CalculateTypeString = (byteValue, addressFamily) =>
        addressFamily switch
        {
            _ when addressFamily == IpAddressFamilyEnum.IpV4 => MessageTypeV4Enum.GetValueById(byteValue).ToString(),
            _ when addressFamily == IpAddressFamilyEnum.IpV6 => MessageTypeV6Enum.GetValueById(byteValue).ToString(),
            _ => "IPAddressFamily not supported!"
        };

    public static readonly Func<int, byte[], int, int, int, int, byte[]> GetBytes =
        (type, originPayload, messageSize, code, checksum, extraOffset) =>
        {
            byte[] data = new byte[messageSize + extraOffset];
            Buffer.BlockCopy(BitConverter.GetBytes(type), 0, data, 0, 1);
            Buffer.BlockCopy(BitConverter.GetBytes(code), 0, data, 1, 1);
            Buffer.BlockCopy(BitConverter.GetBytes(checksum), 0, data, 2, 2);
            Buffer.BlockCopy(originPayload, 0, data, 4, messageSize);
            return data;
        };
}