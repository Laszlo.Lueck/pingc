using System;

namespace pingCs;
internal record MessageTypeV6Enum(string name) : Enumeration(name)
{
    public static MessageTypeV6Enum DestinationUnreachable = new(nameof(DestinationUnreachable));
    public static MessageTypeV6Enum PacketTooBig = new(nameof(PacketTooBig));
    public static MessageTypeV6Enum TimeExceeded = new(nameof(TimeExceeded));
    public static MessageTypeV6Enum PrivateExperimentation100 = new(nameof(PrivateExperimentation100));
    public static MessageTypeV6Enum PrivateExperimentation101 = new(nameof(PrivateExperimentation101));
    public static MessageTypeV6Enum EchoRequest = new(nameof(EchoRequest));
    public static MessageTypeV6Enum EchoReply = new(nameof(EchoReply));
    public static MessageTypeV6Enum MulticastListenerQuery = new(nameof(MulticastListenerQuery));
    public static MessageTypeV6Enum Version1MulticastListenerQuery = new(nameof(Version1MulticastListenerQuery));
    public static MessageTypeV6Enum MulticastListenerDone = new(nameof(MulticastListenerDone));
    public static MessageTypeV6Enum RouterSolicitation = new(nameof(RouterSolicitation));
    public static MessageTypeV6Enum RouterAdvertisement = new(nameof(RouterAdvertisement));
    public static MessageTypeV6Enum NeighborSolicitation = new(nameof(NeighborSolicitation));
    public static MessageTypeV6Enum NeighborAdvertisement = new(nameof(NeighborAdvertisement));
    public static MessageTypeV6Enum Redirect = new(nameof(Redirect));
    public static MessageTypeV6Enum RouterRenumbering = new(nameof(RouterRenumbering));
    public static MessageTypeV6Enum IcmpNodeInformationQuery = new(nameof(IcmpNodeInformationQuery));
    public static MessageTypeV6Enum IcmpNodeInformationResponse = new(nameof(IcmpNodeInformationResponse));

    public static MessageTypeV6Enum InverseNeighborDiscoverySolicitationMessage =
        new(nameof(InverseNeighborDiscoverySolicitationMessage));

    public static MessageTypeV6Enum InverseNeighborDiscoveryAdvertisementMessage =
        new(nameof(InverseNeighborDiscoveryAdvertisementMessage));

    public static MessageTypeV6Enum Version2MulticastListenerReport = new(nameof(Version2MulticastListenerReport));

    public static MessageTypeV6Enum HomeAgentAddressDiscoveryRequestMessage =
        new(nameof(HomeAgentAddressDiscoveryRequestMessage));

    public static MessageTypeV6Enum HomeAgentAddressDiscoveryReplyMessage =
        new(nameof(HomeAgentAddressDiscoveryReplyMessage));

    public static MessageTypeV6Enum MobilePrefixSolicitation = new(nameof(MobilePrefixSolicitation));
    public static MessageTypeV6Enum MobilePrefixAdvertisement = new(nameof(MobilePrefixAdvertisement));

    public static MessageTypeV6Enum CertificationPathSolicitationMessage =
        new(nameof(CertificationPathSolicitationMessage));

    public static MessageTypeV6Enum CertificationPathAdvertisementMessage =
        new(nameof(CertificationPathAdvertisementMessage));

    public static MessageTypeV6Enum IcmpMessagesUtilizedByExperimentalMobilityProtocolsSuchAsSeamoby =
        new(nameof(IcmpMessagesUtilizedByExperimentalMobilityProtocolsSuchAsSeamoby));

    public static MessageTypeV6Enum MulticastRouterAdvertisement = new(nameof(MulticastRouterAdvertisement));
    public static MessageTypeV6Enum MulticastRouterSolicitation = new(nameof(MulticastRouterSolicitation));
    public static MessageTypeV6Enum MulticastRouterTermination = new(nameof(MulticastRouterTermination));
    public static MessageTypeV6Enum RplControlMessage = new(nameof(RplControlMessage));
    public static MessageTypeV6Enum PrivateExperimentation200 = new(nameof(PrivateExperimentation200));
    public static MessageTypeV6Enum PrivateExperimentation201 = new(nameof(PrivateExperimentation201));

    public static MessageTypeV6Enum ReservedForExpansionOfIcmpV6InformationalMessages =
        new(nameof(ReservedForExpansionOfIcmpV6InformationalMessages));

    public static MessageTypeV6Enum GetValueById(int id)
    {
        return id switch
        {
            1 => DestinationUnreachable,
            2 => PacketTooBig,
            3 => TimeExceeded,
            100 => PrivateExperimentation100,
            101 => PrivateExperimentation101,
            128 => EchoRequest,
            129 => EchoReply,
            130 => MulticastListenerQuery,
            131 => Version1MulticastListenerQuery,
            132 => MulticastListenerDone,
            133 => RouterSolicitation,
            134 => RouterAdvertisement,
            135 => NeighborSolicitation,
            136 => NeighborAdvertisement,
            137 => Redirect,
            138 => RouterRenumbering,
            139 => IcmpNodeInformationQuery,
            140 => IcmpNodeInformationResponse,
            141 => InverseNeighborDiscoverySolicitationMessage,
            142 => InverseNeighborDiscoveryAdvertisementMessage,
            143 => Version2MulticastListenerReport,
            144 => HomeAgentAddressDiscoveryRequestMessage,
            145 => HomeAgentAddressDiscoveryReplyMessage,
            146 => MobilePrefixSolicitation,
            147 => MobilePrefixAdvertisement,
            148 => CertificationPathSolicitationMessage,
            149 => CertificationPathAdvertisementMessage,
            150 => IcmpMessagesUtilizedByExperimentalMobilityProtocolsSuchAsSeamoby,
            151 => MulticastRouterAdvertisement,
            152 => MulticastRouterSolicitation,
            153 => MulticastRouterTermination,
            154 => RplControlMessage,
            200 => PrivateExperimentation200,
            201 => PrivateExperimentation201,
            255 => ReservedForExpansionOfIcmpV6InformationalMessages,
            _ => throw new ArgumentOutOfRangeException(nameof(id), id, null)
        };
    }

    public override string ToString()
    {
        return base.ToString();
    }
}