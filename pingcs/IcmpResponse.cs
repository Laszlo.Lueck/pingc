using System;
using System.Collections.Immutable;
using System.Net;
using System.Net.Sockets;

namespace pingCs;
internal class IcmpResponse
{
    internal IpAddressFamilyEnum? AddressFamily;
    public double RoundTripTime;
    public string? TypeString;
    protected byte Type;
    public IPEndPoint? Origin;
    public IPEndPoint? FromIp;
    public int Code;
    public int PayloadSize;
    public ImmutableArray<byte> Data;
}

internal class IcmpResponseV4 : IcmpResponse
{
    public readonly int Ttl;
    public int Checksum;
    public int Identifier;
    public readonly MessageTypeV4Enum TypeV4Enum;
    public int Sequence;
    
    internal IcmpResponseV4(IPEndPoint origin, int payloadSize, byte[] data, IPEndPoint? fromIp,
        IpAddressFamilyEnum addressFamily, double roundTripTime)
    {
        Origin = origin;
        PayloadSize = payloadSize;
        Data = data.ToImmutableArray();
        Type = addressFamily == IpAddressFamilyEnum.IpV4 ? data[20] : data[0];
        TypeString = StaticNetUtils.CalculateTypeString(Type, addressFamily);
        TypeV4Enum = MessageTypeV4Enum.GetValueById(Type);
        Code = addressFamily == IpAddressFamilyEnum.IpV4 ? data[21] : data[1];
        FromIp = fromIp;
        AddressFamily = addressFamily;
        RoundTripTime = roundTripTime;
        Checksum = BitConverter.ToUInt16(data, 22);
        Identifier = BitConverter.ToInt16(data, 0);
        Sequence = BitConverter.ToInt16(data, 2);
        Ttl = data[8];
    }
    
}

internal class IcmpResponseV6 : IcmpResponse
{
    internal readonly MessageTypeV6Enum TypeV6Enum;
    
    internal IcmpResponseV6(IPEndPoint origin, int payloadSize, byte[] data, IPEndPoint? fromIp,
        IpAddressFamilyEnum addressFamily, double roundTripTime)
    {
        Origin = origin;
        PayloadSize = payloadSize;
        Data = data.ToImmutableArray();
        Type = addressFamily == IpAddressFamilyEnum.IpV4 ? data[20] : data[0];
        TypeString = StaticNetUtils.CalculateTypeString(Type, addressFamily);
        TypeV6Enum = MessageTypeV6Enum.GetValueById(Type);
        Code = addressFamily == IpAddressFamilyEnum.IpV4 ? data[21] : data[1];
        FromIp = fromIp;
        AddressFamily = addressFamily;
        RoundTripTime = roundTripTime;
    }
}