﻿using System;
using System.Threading;
using System.Runtime;
using static System.Console;

namespace pingCs;

internal abstract class PingC
{
    private static void Main(string[] args)
    {
        var argumentParameter = StaticUtils.ResolveArgs(args);

        if (argumentParameter is null)
        {
            WriteLine(StaticUtils.WriteHelpText());
            Environment.Exit(0);
        }

        var request = new IcmpRequest(argumentParameter.Ttl, argumentParameter.IpOrHostName,
            argumentParameter.TimeoutInMs, true, argumentParameter.GivenAddressType);
        
        WriteLine(
            $"pinging {argumentParameter.IpOrHostName} ({request.RealTargetHostName}) from {request.IpEndPointSource?.Address.ToString() ?? "---"} for {argumentParameter.Count} times with {argumentParameter.TimeBetweenPing}s delay");

        for (var i = 1; i <= argumentParameter.Count; i++)
        {
            var response = IcmpConnector.Ping(request);
            switch (response.AddressFamily)
            {
                case var value when value == IpAddressFamilyEnum.IpV4:
                    var respV4 = (IcmpResponseV4) response;
                    WriteLine(
                        $"[{i}] :: {respV4.TypeV4Enum} from {respV4.Origin?.Address.ToString() ?? "---"} to {respV4.FromIp?.Address.ToString() ?? "---"} in {respV4.RoundTripTime} ms with TTL {respV4.Ttl}");
                    break;
                case var value when value == IpAddressFamilyEnum.IpV6:
                    var respV6 = (IcmpResponseV6) response;
                    WriteLine(
                        $"[{i}] :: {respV6.TypeV6Enum} from {respV6.Origin?.Address.ToString() ?? "---"} to {respV6.FromIp?.Address.ToString() ?? "---"} in {respV6.RoundTripTime} ms");
                    break;
                default:
                    throw new ArgumentException("invalid ip address family type", nameof(response.AddressFamily));
            }

            if (argumentParameter.TimeBetweenPing > 0)
                Thread.Sleep((int) (argumentParameter.TimeBetweenPing * 1000));
        }

        Environment.Exit(0);
    }
}