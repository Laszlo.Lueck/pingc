namespace pingCs;
internal abstract record Enumeration(string name)
{
    private string Name { get; } = name;

    public override string ToString() => Name;
    
}