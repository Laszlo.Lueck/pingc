using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime;
using static System.Console;
using System.Threading;

namespace pingCs;
internal static class StaticUtils
{
    public static ArgumentParameter? ResolveArgs(string[]? args)
    {
        if (args == null || args.Length == 0)
            return null;

        if (args[0] == "help")
            return null;
        
        var ipAddress = args[0];
        var tVal = 3;
        decimal cVal = 0;
        byte vVal = 128;
        var aVal = 1000;
        var xVal = 0;

        for (var i = 1; i < args.Length; i++)
        {
            switch (args[i])
            {
                case var tc when tc.StartsWith("t="):
                    var intValOpt = tc
                        .Replace("t=", "")
                        .Trim();
                    if (int.TryParse(intValOpt, out var foo))
                        tVal = foo;
                    break;
                case var xc when xc.StartsWith("c="):
                    var sep = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    var intCOpt = xc
                        .Replace("c=", "")
                        .Replace(".", sep)
                        .Replace(",", sep)
                        .Trim();
                    if (decimal.TryParse(intCOpt, out var c))
                        cVal = c;
                    break;
                case var ac when ac.StartsWith("a="):
                    var aValOpt = ac
                        .Replace("a=", "")
                        .Trim();
                    if (int.TryParse(aValOpt, out var aValX))
                        aVal = aValX;
                    break;
                case var vc when vc.StartsWith("v="):
                    var vValOpt = vc
                        .Replace("v=", "")
                        .Trim();
                    if (byte.TryParse(vValOpt, out var vValX))
                        vVal = vValX;
                    break;
                case var xc when xc.StartsWith("x="):
                    var xValOpt = xc
                        .Replace("x=", "")
                        .Trim();
                    if (int.TryParse(xValOpt, out var xValX) && xValX is 4 or 6)
                        xVal = xValX;
                    break;
            }
        }

        return new ArgumentParameter(ipAddress, tVal, cVal, aVal, vVal, xVal);
    }


    public static string WriteHelpText()
    {
        var nl = Environment.NewLine;
        var returnText = $"pingcs, a ping alternative in c#, native for various platforms. {nl}";
        returnText += $"written 2024 by L. Lueck {nl}";
        returnText += $"it use bflat for native os compiling and upx for executable compression{nl}";
        returnText += $"Bflat: https://flattened.net/ {nl}";
        returnText += $"UPX: https://upx.github.io/ {nl}";
        returnText += $"{nl}";
        returnText += $"usage of pingcs: {nl}";
        returnText += $"pingcs [IPAddress or HostName or help] {nl}";
        returnText += $"help - displays this help text (same as pingcs without any parameter){nl}";
        returnText += $"option t [int 1..n] count of pings | if not given, 3 ping per default {nl}";
        returnText +=
            $"option c [dec [0,0 | 0.0] .. [n,n | n.n]] default wait time in seconds between ping commands, default is 0 {nl}";
        returnText +=
            $"option a [int 0..n] time in millisecond how long the ping tries to connect to the endpoint, default is 1000{nl}";
        returnText += $"option v [byte 0..255] ttl for ping, defines the rfc-792 ttl{nl}";
        returnText += $"option x [int 4 or 6] sets the address type to be used manually 4 is IpV4 6 is IpV6 not given is best default{nl}";
        returnText += $"--------------------{nl}";
        returnText += $"simple usage: pingcs 1.1.1.1 (pings the cloudflare dns three times){nl}";
        return returnText;
    }
}