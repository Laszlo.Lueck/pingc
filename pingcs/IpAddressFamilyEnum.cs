namespace pingCs;

internal record IpAddressFamilyEnum(string name) : Enumeration(name)
{
    public static readonly IpAddressFamilyEnum IpV4 = new(nameof(IpV4));
    public static readonly IpAddressFamilyEnum IpV6 = new(nameof(IpV6));

    public override string ToString()
    {
        return base.ToString();
    }
}