using System;
using System.Runtime;
using System.Linq;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

namespace pingCs;

internal class IcmpRequest
{
    public readonly int Ttl;
    public readonly byte[] Payload = new byte[1024];
    public readonly byte Type;
    public readonly int Code;
    public readonly int Checksum;
    public readonly int MessageSize;
    public readonly int PacketSize;
    public readonly int ReceiveTimeout;
    public readonly bool DontFragment;
    public readonly IpAddressFamilyEnum? AddressFamily;
    public readonly IPEndPoint? IpEndPointSource;
    public readonly IPEndPoint? IpEndPointTarget;
    public readonly string? RealTargetHostName;


    public IcmpRequest(int ttl, string endPoint, int receiveTimeout, bool dontFragment, int manualAddressType)
    {
        var (returnAddressType, ipEndpointTarget) = _getEndpointFromString(endPoint, manualAddressType);
        ArgumentNullException.ThrowIfNull(ipEndpointTarget);
        
        IpEndPointTarget = ipEndpointTarget;
        manualAddressType = returnAddressType;

        AddressFamily = manualAddressType switch
        {
            4 => IpAddressFamilyEnum.IpV4,
            6 => IpAddressFamilyEnum.IpV6,
            _ => _getIpAddressFamily(IpEndPointTarget.AddressFamily)
        };

        try
        {
            RealTargetHostName = Dns.GetHostEntry(IpEndPointTarget.Address).HostName;
        }
        catch
        {
            RealTargetHostName = IpEndPointTarget.Address.ToString();
        }


        IpEndPointSource = new IPEndPoint(_getFromHostOrIp(AddressFamily), 0);
        
        ArgumentNullException.ThrowIfNull(IpEndPointSource);
        Type = _calculateType(AddressFamily);

        DontFragment = dontFragment;
        Ttl = ttl;
        ReceiveTimeout = receiveTimeout;
        Code = 0;
        Buffer.BlockCopy(BitConverter.GetBytes((short) 1), 0, Payload, 0, 2);
        Buffer.BlockCopy(BitConverter.GetBytes((short) 1), 0, Payload, 2, 2);
        var data = _generateRandomData(32);
        Buffer.BlockCopy(data, 0, Payload, 4, data.Length);
        MessageSize = data.Length + 4;
        PacketSize = MessageSize + 4;
        Checksum = _getChecksum(MessageSize, StaticNetUtils.GetBytes(Type, Payload, MessageSize, Code, Checksum, 9));
    }
    
    private readonly Func<AddressFamily, IpAddressFamilyEnum> _getIpAddressFamily = addressFamily =>
        addressFamily switch
        {
            System.Net.Sockets.AddressFamily.InterNetwork => IpAddressFamilyEnum.IpV4,
            System.Net.Sockets.AddressFamily.InterNetworkV6 => IpAddressFamilyEnum.IpV6,
            _ => throw new InvalidEnumArgumentException()
        };


    private readonly Func<IpAddressFamilyEnum, IPAddress> _getFromHostOrIp =
        addressFamily =>
        {
            var al = (Dns.GetHostEntry(Dns.GetHostName())).AddressList;
            return addressFamily switch
            {
                _ when addressFamily == IpAddressFamilyEnum.IpV4 => al.First(i =>
                    i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork),
                _ when addressFamily == IpAddressFamilyEnum.IpV6 => al.First(
                    i => i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6),
                _ => throw new ArgumentOutOfRangeException(nameof(addressFamily), addressFamily,
                    "Unresolvable parameter given")
            };
        };

    private readonly Func<IpAddressFamilyEnum, byte> _calculateType = addressFamily =>
        addressFamily switch
        {
            _ when addressFamily == IpAddressFamilyEnum.IpV4 => 0x08,
            _ when addressFamily == IpAddressFamilyEnum.IpV6 => 0x80,
            _ => throw new InvalidEnumArgumentException()
        };


    private readonly Func<string, int, (int, IPEndPoint?)> _getEndpointFromString =
        (endpointAsString, addressFamily) =>
            IPAddress.TryParse(endpointAsString, out var ipAddress)
                ? (addressFamily, new IPEndPoint(ipAddress, 0))
                : ResolveIpEndPoint(endpointAsString, addressFamily);


    private readonly Func<int, byte[]> _generateRandomData = length =>
    {
        var data = new byte[length];
        RandomNumberGenerator
            .Create()
            .GetBytes(data);
        return data;
    };

    private static readonly Func<string, int, (int, IPEndPoint?)> ResolveIpEndPoint = (stringIpOrHost, family) =>
    {
        var hostIpAddresses = Dns.GetHostAddresses(stringIpOrHost);
        if (hostIpAddresses.Length is 0)
            return (family, null);

        var hostAddress = family switch
        {
            4 => hostIpAddresses.FirstOrDefault(d => d.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork),
            6 => hostIpAddresses.FirstOrDefault(d =>
                d.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6),
            _ => hostIpAddresses.FirstOrDefault()
        };

        if (hostAddress is not null || family != 6)
            return hostAddress is not null
                ? (family, new IPEndPoint(hostAddress, 0))
                : (family, null);
        {
            hostAddress =
                hostIpAddresses.FirstOrDefault(d => d.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
            if (hostAddress is not null)
                return (4, new IPEndPoint(hostAddress, 0));
        }

        return (family, null);
    };

    private readonly Func<int, byte[], ushort> _getChecksum = (messageSize, payload) =>
    {
        var packetSize = messageSize + 8;
        var checksum = Enumerable.Range(0, packetSize / 2)
            .Select(index => BitConverter.ToUInt16(payload, index * 2))
            .Aggregate(0u, (sum, value) => sum + value);

        checksum = (checksum >> 16) + (checksum & 0xffff);
        checksum += (checksum >> 16);
        return (ushort) ~checksum;
    };
}