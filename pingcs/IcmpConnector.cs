using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Linq;

namespace pingCs;

internal static class IcmpConnector
{
    internal static IcmpResponse Ping(IcmpRequest icmpRequest)
    {
        ArgumentNullException.ThrowIfNull(icmpRequest);

        using var host = icmpRequest.AddressFamily switch
        {
            _ when icmpRequest.AddressFamily == IpAddressFamilyEnum.IpV4 => new Socket(AddressFamily.InterNetwork,
                SocketType.Raw, ProtocolType.Icmp),
            _ when icmpRequest.AddressFamily == IpAddressFamilyEnum.IpV6 => new Socket(AddressFamily.InterNetworkV6,
                SocketType.Raw, ProtocolType.IcmpV6),
            _ => throw new InvalidEnumArgumentException()
        };

        switch (icmpRequest.AddressFamily)
        {
            case var value when value == IpAddressFamilyEnum.IpV4:
                host.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.IpTimeToLive, icmpRequest.Ttl);
                if (!RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    host.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.DontFragment, icmpRequest.DontFragment);
                break;
            case var value when value == IpAddressFamilyEnum.IpV6:
                host.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IpTimeToLive, icmpRequest.Ttl);
                if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    host.DualMode = false;
                break;
            default:
                throw new InvalidEnumArgumentException();
        }

        ArgumentNullException.ThrowIfNull(icmpRequest.IpEndPointTarget);
        ArgumentNullException.ThrowIfNull(icmpRequest.IpEndPointSource);

        host.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, icmpRequest.ReceiveTimeout);

        var sendPayload = StaticNetUtils.GetBytes(icmpRequest.Type, icmpRequest.Payload, icmpRequest.MessageSize + 4,
            icmpRequest.Code, icmpRequest.Checksum, 4);

        var returnedPayload = new byte[1024];

        EndPoint ep = icmpRequest.IpEndPointSource;
        var sw = Stopwatch.StartNew();
        host.SendTo(sendPayload, icmpRequest.PacketSize, SocketFlags.None, icmpRequest.IpEndPointTarget);
        var received = host.ReceiveFrom(returnedPayload, ref ep);
        sw.Stop();
        host.Close();
        host.Dispose();
        var receivedEndpoint = icmpRequest.IpEndPointTarget;

        return icmpRequest.AddressFamily switch
        {
            _ when icmpRequest.AddressFamily == IpAddressFamilyEnum.IpV4 => new IcmpResponseV4(receivedEndpoint,
                received, returnedPayload.Take(received).ToArray(), icmpRequest.IpEndPointSource,
                icmpRequest.AddressFamily, sw.Elapsed.TotalMilliseconds),
            _ when icmpRequest.AddressFamily == IpAddressFamilyEnum.IpV6 => new IcmpResponseV6(receivedEndpoint,
                received, returnedPayload.Take(received).ToArray(), icmpRequest.IpEndPointSource,
                icmpRequest.AddressFamily, sw.Elapsed.TotalMilliseconds),
            _ => throw new InvalidEnumArgumentException()
        };
    }
}