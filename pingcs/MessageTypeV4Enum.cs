using System;

namespace pingCs;
internal record MessageTypeV4Enum(string name) : Enumeration(name)
{
        public static MessageTypeV4Enum EchoReply = new(nameof(EchoReply));
        public static MessageTypeV4Enum DestinationUnreachable = new(nameof(DestinationUnreachable));
        public static MessageTypeV4Enum SourceQuench = new(nameof(SourceQuench));
        public static MessageTypeV4Enum Redirect = new(nameof(Redirect));
        public static MessageTypeV4Enum EchoRequest = new(nameof(EchoRequest));
        public static MessageTypeV4Enum TimeExceeded = new(nameof(TimeExceeded));
        public static MessageTypeV4Enum ParameterProblem = new(nameof(ParameterProblem));
        public static MessageTypeV4Enum Timestamp = new(nameof(Timestamp));
        public static MessageTypeV4Enum TimestampReply = new(nameof(TimestampReply));
        public static MessageTypeV4Enum InformationRequest = new(nameof(InformationRequest));
        public static MessageTypeV4Enum InformationReply = new(nameof(InformationReply));
        public static MessageTypeV4Enum GetValueById(int id)
        {
                return id switch
                {
                        0 => EchoReply,
                        3 => DestinationUnreachable,
                        4 => SourceQuench,
                        5 => Redirect,
                        8 => EchoRequest,
                        11 => TimeExceeded,
                        12 => ParameterProblem,
                        13 => Timestamp,
                        14 => TimestampReply,
                        15 => InformationRequest,
                        16 => InformationReply,
                        _ => throw new ArgumentOutOfRangeException(nameof(id), id, null)
                };
        }

        public override string ToString()
        {
                return base.ToString();
        }
}