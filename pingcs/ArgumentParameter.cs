namespace pingCs;

internal record ArgumentParameter(string IpOrHostName, int Count, decimal TimeBetweenPing, int TimeoutInMs, byte Ttl, int GivenAddressType);